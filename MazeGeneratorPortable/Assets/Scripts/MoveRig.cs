﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRig : MonoBehaviour {

    public GameObject rig;

    private GameObject _stats;
    private Stats _statsSc;

    private void Start()
    {
	    _stats = GameObject.Find("StatSaver");
	    _statsSc = _stats.GetComponent<Stats>();
    }

    public void RigToMenu()
    {
		StartCoroutine (ExecuteAfterTime (0.5f));
    }

	IEnumerator ExecuteAfterTime(float time)
	{
		yield return new WaitForSeconds(time);

		StartCoroutine("DoTheTp");
	}


	private void DoTheTp(){
		rig.transform.position = new Vector3(-10,0,-10);
		_statsSc.SetEndTime();
		_statsSc.UpdateStat();
	}
}
