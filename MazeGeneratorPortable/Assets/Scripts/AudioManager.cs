﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{

	public Sound[] Sounds;
	private Sound playing;
	public bool on = true;
	
	// Use this for initialization
	void Awake () {
		foreach (var sound in Sounds)
		{
			sound.Source = gameObject.AddComponent<AudioSource>();
			sound.Source.clip = sound.clip;
			sound.Source.volume = sound.volume;
			sound.Source.pitch = sound.pitch;
			sound.Source.loop = sound.loop;
		}
	}


	public void Play(int n)
	{
		playing = Sounds[n];
		playing.Source.Play();
	}

	public void Stop()
	{
		if (on) playing.Source.Stop();
	}

	public void ToggleMusic()
	{
		on = !on;
	}

	public void PlayRandomMusic()
	{
		if (on)
		{
			int n = Random.Range(0, Sounds.Length);
			Play(n);
		}
	}
}
