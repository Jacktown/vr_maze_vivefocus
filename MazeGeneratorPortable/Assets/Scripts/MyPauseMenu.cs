﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyPauseMenu : MonoBehaviour {

    public GameObject rightController;
    public GameObject leftController;

    public GameObject mainMenu;
    public GameObject transitionMenu;
    public GameObject inventory;

    public Scene resetScene;

    public void TogglePauseMenu()
    {
        bool state = this.gameObject.activeSelf;
        this.gameObject.SetActive(!state);
        
        //inventory.SetActive(state);
        
        UserControllerListener ucl = rightController.GetComponent<UserControllerListener>();
        ucl.ActivateTool(state);
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(resetScene.name);
    }
}
