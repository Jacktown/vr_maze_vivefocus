﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Screenshots : MonoBehaviour {

    public GameObject camMap;
    public Image imageUI;
	private Camera camTr;

    private void Start()
    {
        
    }

	public void TakePicture(int w, int h)
    {
        camMap.SetActive(true);

		// adapt the zoom according to maze dimensions
		camTr = camMap.GetComponent<Camera> ();
		int sizeScale = (w > h) ? w : h;
		camTr.orthographicSize = 0.7f * sizeScale - 1.0f;

        StartCoroutine(ExecuteAfterTime(2f));
    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        StartCoroutine(ReadScreen());
    }

    IEnumerator ReadScreen()
    {
        yield return new WaitForEndOfFrame();

		Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
		tex.ReadPixels (new Rect(0,0, Screen.width, Screen.height), 5, 5);
        tex.Apply();
		//Debug.Log ("Picture Taken");
        Sprite levelMap = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0), 100.0f);
        imageUI.sprite = levelMap;
        camMap.SetActive(false);
    }
}

