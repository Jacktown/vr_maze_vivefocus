﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderToText : MonoBehaviour {
    
    public Text heightText;
    public Text widthText;
    public Text exitText;

    public void SetWidthText(float newWidthText)
    {
        widthText.text = "" + newWidthText;
    }

    public void SetHeightText(float newHeightText)
    {
        heightText.text = "" + newHeightText;
    }

    public void SetExitText(float newExitText)
    {
        exitText.text = "" + newExitText;
    }
}
