﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class WallGeneration : MonoBehaviour
{
    [Header("Standard Settings")]
    [Range(10, 50)]
    public int width = 20;
    [Range(10, 50)]
    public int height = 20;
    [Range(2f, 15f)]
    public float depth = 5;
    [Header("Advanced Settings")]
    [Range(0, 100)]
    public int horizontalPercentage = 45;
    [Range(0, 100)]
    public int verticalPercentage = 90;
    [Range(0, 100)]
    public int roomPercentage = 0;

    [Header("Assets")]
    public GameObject block;

    int[,] wallHorizontal;
    int[,] wallVertical;
    float[,] heightMap;
    float scale = 5;
    int nbWall = 0;
    float minDepth = 2f;

    struct Position
    {
        public int posX;
        public int posY;

        public Position(int x, int y)
        {
            posX = x;
            posY = y;
        }
    }

    public GameObject GenerateWall()
    {
        int nbTry = 0;
        List<List<Position>> regions;
        //Debug.Log("Start [GenerateWall]" + Time.realtimeSinceStartup);
        wallHorizontal = new int[width, height + 1];
        wallVertical = new int[width + 1, height];
        heightMap = new float[width, height];
        FillMap();
        FixMap();
        regions = GetRegions();
        nbTry++;
        DrawBlocks(regions);
        //Debug.Log("Number of regions: " + regions.Count);
        //Debug.Log("Size of biggest region: " + regions[regions.Count - 1].Count);
        //Debug.Log("Size of smallest region: " + regions[0].Count);
        //Debug.Log("Room treshold: " + width * height * (roomPercentage / 100.0f));
        //Debug.Log("Number of tries: " + nbTry);
        //Debug.Log("Score: " + score);
        return Make3D();
    }

    void FillMap() // comment : cf MapGeneration
    {
        for (int y = 0; y < wallHorizontal.GetLength(1); y++)
        {
            for (int x = 0; x < wallHorizontal.GetLength(0); x++)
            {
                if (y == 0 || y == wallHorizontal.GetLength(1) - 1)
                {
                    wallHorizontal[x, y] = 1;
                }
                else
                {
                    wallHorizontal[x, y] = (UnityEngine.Random.Range(0, 100) < horizontalPercentage) ? 1 : 0;
                }
            }
        }

        for (int y = 0; y < wallVertical.GetLength(1); y++)
        {
            for (int x = 0; x < wallVertical.GetLength(0); x++)
            {
                if (x == 0 || x == wallVertical.GetLength(0) - 1)
                {
                    wallVertical[x, y] = 1;
                }
                else
                {
                    switch (wallHorizontal[x - 1, y] + wallHorizontal[x - 1, y + 1] + wallHorizontal[x, y] + wallHorizontal[x, y + 1])
                    {
                        case 0:
                            wallVertical[x, y] = 1;
                            break;
                        case 1:
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage) ? 1 : 0;
                            break;
                        case 2:
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage / 2) ? 1 : 0;
                            break;
                        case 3:
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage / 4) ? 1 : 0;
                            break;
                        case 4:
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage / 8) ? 1 : 0;
                            break;
                        default:
                            wallVertical[x, y] = wallVertical[x, y];
                            break;
                    }
                }
            }
        }
    }

    List<List<Position>> GetRegions()
    {
        List<List<Position>> regions = new List<List<Position>>();
        int[,] mapFlags = new int[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (mapFlags[x, y] == 0)
                {
                    List<Position> newRegion = GetRegion(x, y);
                    regions.Add(newRegion);

                    foreach (Position tile in newRegion)
                    {
                        mapFlags[tile.posX, tile.posY] = 1;
                    }
                }
            }
        }
        return regions;
    }

    List<Position> GetRegion(int startX, int startY)
    {
        List<Position> region = new List<Position>();
        int[,] mapFlags = new int[width, height];

        Queue<Position> toVisit = new Queue<Position>();
        toVisit.Enqueue(new Position(startX, startY));
        mapFlags[startX, startY] = 1;

        while (toVisit.Count > 0)
        {
            Position tile = toVisit.Dequeue();
            region.Add(tile);

            if (IsInMapRange(tile.posX, tile.posY - 1) && mapFlags[tile.posX, tile.posY - 1] == 0 && wallHorizontal[tile.posX, tile.posY] == 0)
            {
                mapFlags[tile.posX, tile.posY - 1] = 1;
                toVisit.Enqueue(new Position(tile.posX, tile.posY - 1));
            }
            if (IsInMapRange(tile.posX - 1, tile.posY) && mapFlags[tile.posX - 1, tile.posY] == 0 && wallVertical[tile.posX, tile.posY] == 0)
            {
                mapFlags[tile.posX - 1, tile.posY] = 1;
                toVisit.Enqueue(new Position(tile.posX - 1, tile.posY));
            }
            if (IsInMapRange(tile.posX, tile.posY + 1) && mapFlags[tile.posX, tile.posY + 1] == 0 && wallHorizontal[tile.posX, tile.posY + 1] == 0)
            {
                mapFlags[tile.posX, tile.posY + 1] = 1;
                toVisit.Enqueue(new Position(tile.posX, tile.posY + 1));
            }
            if (IsInMapRange(tile.posX + 1, tile.posY) && mapFlags[tile.posX + 1, tile.posY] == 0 && wallVertical[tile.posX + 1, tile.posY] == 0)
            {
                mapFlags[tile.posX + 1, tile.posY] = 1;
                toVisit.Enqueue(new Position(tile.posX + 1, tile.posY));
            }
        }
        return region;
    }

    void DrawBlocks(List<List<Position>> _regions)
    {
        for (int i = 0; i < _regions.Count; i++)
        {
            List<Position> newRegion = _regions[i];
            float _depth = UnityEngine.Random.Range(minDepth, depth);
            foreach (Position tile in newRegion)
            {
                heightMap[tile.posX, tile.posY] = _depth;
            }
        }
    }

    GameObject Make3D()
    {
        GameObject newWall = new GameObject("Wall" + nbWall++);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Vector3 pos = new Vector3(x * scale + scale / 2, scale / 2, y * scale + scale / 2);
                GameObject newBlock = Instantiate(block, pos, Quaternion.Euler(0, 0, 0));
                newBlock.transform.localScale = new Vector3(scale, heightMap[x, y], scale);
                newBlock.transform.parent = newWall.transform;
            }
        }
        newWall.transform.rotation = Quaternion.Euler(-90, 0, 0);
        newWall.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        newWall.AddComponent<MeshCombiner>();
        var boxCollider = newWall.AddComponent<BoxCollider>();
        boxCollider.center = new Vector3(scale / 2, 0.12f, scale / 2);
        boxCollider.size = new Vector3(scale, (float)depth / 20f, scale);
        //Debug.Log("End [GenerateWall]" + Time.realtimeSinceStartup);
        return newWall;
    }

	void FixMap() // comment : cf MapGeneration
    {
        int isolatedCountH = 0;
        int isolatedCountV = 0;
        for (int y = 0; y < wallHorizontal.GetLength(1); y++)
        {
            for (int x = 0; x < wallHorizontal.GetLength(0); x++)
            {
                if (IsIsolated(x, y, false) && wallHorizontal[x, y] == 1)
                {
                    isolatedCountH++;
                    LinkToNeighbour(x, y, false);
                }
            }
        }

        for (int y = 0; y < wallVertical.GetLength(1); y++)
        {
            for (int x = 0; x < wallVertical.GetLength(0); x++)
            {
                if (IsIsolated(x, y, true) && wallVertical[x, y] == 1)
                {
                    isolatedCountV++;
                    LinkToNeighbour(x, y, true);
                }
            }
        }
        //Debug.Log("Isolated Horizontal walls: " + isolatedCountH + " ; Isolated Vertical walls: " + isolatedCountV);
        //Debug.Log("Horizontal walls: " + CountHorizontalWalls() + " ; Vertical walls: " + CountVerticalWalls());
    }

    int CountHorizontalWalls()
    {
        int horiCount = 0;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height + 1; y++)
            {
                if (wallHorizontal[x, y] != 0)
                {
                    horiCount++;
                }
            }
        }
        return horiCount;
    }

    int CountVerticalWalls()
    {
        int vertCount = 0;
        for (int x = 0; x < width + 1; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (wallVertical[x, y] != 0)
                {
                    vertCount++;
                }
            }
        }
        return vertCount;
    }

    bool IsIsolated(int x, int y, bool vertical)
    {
        bool ret = false;
        if (vertical)
        {
            if (x != 0 && x != wallVertical.GetLength(0) - 1 && y != 0 && y != wallVertical.GetLength(1) - 1)
            {
                if (wallHorizontal[x, y] == 0 && wallHorizontal[x, y + 1] == 0 && wallHorizontal[x - 1, y] == 0 && wallHorizontal[x - 1, y + 1] == 0 && wallVertical[x, y - 1] == 0 && wallVertical[x, y + 1] == 0)
                    ret = true;
            }
        }
        else
        {
            if (y != 0 && y != wallHorizontal.GetLength(1) - 1 && x != 0 && x != wallHorizontal.GetLength(0) - 1)
            {
                if (wallVertical[x, y] == 0 && wallVertical[x, y - 1] == 0 && wallVertical[x + 1, y] == 0 && wallVertical[x + 1, y - 1] == 0 && wallHorizontal[x - 1, y] == 0 && wallHorizontal[x + 1, y] == 0)
                    ret = true;
            }
        }
        return ret;
    }

    void LinkToNeighbour(int x, int y, bool vertical)
    {
        if (CountHorizontalWalls() > CountVerticalWalls())    //add vertical walls
        {
            if (vertical)
            {
                if (UnityEngine.Random.Range(0, 100) < 50)
                {
                    wallVertical[x, y + 1] = 2;
                }
                else
                {
                    wallVertical[x, y - 1] = 2;
                }
            }
            else
            {
                bool solved = false;
                while (!solved)
                {
                    switch (UnityEngine.Random.Range(0, 4))
                    {
                        case 0:
                            if (wallHorizontal[x - 1, y - 1] == 1 || wallHorizontal[x, y - 1] == 1 || IsReachableVertical(x, y - 2))
                            {
                                solved = true;
                                wallVertical[x, y - 1] = 2;
                            }
                            break;
                        case 1:
                            if (wallHorizontal[x - 1, y + 1] == 1 || wallHorizontal[x, y + 1] == 1 || IsReachableVertical(x, y + 1))
                            {
                                solved = true;
                                wallVertical[x, y] = 2;
                            }
                            break;
                        case 2:
                            if (wallHorizontal[x + 1, y + 1] == 1 || wallHorizontal[x, y + 1] == 1 || IsReachableVertical(x + 1, y + 1))
                            {
                                solved = true;
                                wallVertical[x + 1, y] = 2;
                            }
                            break;
                        case 3:
                            if (wallHorizontal[x + 1, y - 1] == 1 || wallHorizontal[x, y - 1] == 1 || IsReachableVertical(x + 1, y - 2))
                            {
                                solved = true;
                                wallVertical[x + 1, y - 1] = 2;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        else   //add horizontal walls
        {
            if (vertical)
            {
                bool solved = false;
                while (!solved)
                {
                    switch (UnityEngine.Random.Range(0, 4))
                    {
                        case 0:
                            if (wallVertical[x - 1, y - 1] == 1 || wallVertical[x - 1, y] == 1 || IsReachableHorizontal(x - 2, y))
                            {
                                solved = true;
                                wallHorizontal[x - 1, y] = 2;
                            }
                            break;
                        case 1:
                            if (wallVertical[x - 1, y + 1] == 1 || wallVertical[x - 1, y] == 1 || IsReachableHorizontal(x - 2, y + 1))
                            {
                                solved = true;
                                wallHorizontal[x - 1, y + 1] = 2;
                            }
                            break;
                        case 2:
                            if (wallVertical[x + 1, y + 1] == 1 || wallVertical[x + 1, y] == 1 || IsReachableHorizontal(x + 1, y + 1))
                            {
                                solved = true;
                                wallHorizontal[x, y + 1] = 2;
                            }
                            break;
                        case 3:
                            if (wallVertical[x + 1, y - 1] == 1 || wallVertical[x + 1, y] == 1 || IsReachableHorizontal(x + 1, y))
                            {
                                solved = true;
                                wallHorizontal[x, y] = 2;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                if (UnityEngine.Random.Range(0, 100) < 50)
                {
                    wallHorizontal[x + 1, y] = 2;
                }
                else
                {
                    wallHorizontal[x - 1, y] = 2;
                }
            }
        }
    }

    bool IsReachableVertical(int x, int y)
    {
        bool ret = false;
        if (y < wallVertical.GetLength(1) && y >= 0)
        {
            if (wallVertical[x, y] == 1)
                ret = true;
        }
        return ret;
    }

    bool IsReachableHorizontal(int x, int y)
    {
        bool ret = false;
        if (x < wallHorizontal.GetLength(0) && x >= 0)
        {
            if (wallHorizontal[x, y] == 1)
                ret = true;
        }
        return ret;
    }

    bool IsInMapRange(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

}
