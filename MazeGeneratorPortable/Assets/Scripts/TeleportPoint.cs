﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TeleportPoint : MonoBehaviour,
	IPointerEnterHandler,
	IPointerExitHandler,
	IPointerHoverHandler
{

	public Material hoveredMaterial;
	public Material defaultMaterial;
	private Renderer _teleportRenderer;
	public UserControllerListener scUCL;



	private void Start()
	{
		_teleportRenderer = GetComponent<Renderer>();
		_teleportRenderer.material = defaultMaterial;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		_teleportRenderer.material = hoveredMaterial;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		_teleportRenderer.material = defaultMaterial;
	}

	public void OnPointerHover(PointerEventData eventData)
	{
		if (scUCL.IsTriggerPressed())
		{
			scUCL.rig.transform.position = transform.position;
			StartCoroutine(scUCL.DisableTeleportation());
		}
	}

}
