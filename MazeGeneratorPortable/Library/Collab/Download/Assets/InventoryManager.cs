﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
	public GameObject[] selectors;
	public GameObject[] items;
	private int[] _quantity;
	public Text[] quantityInfo;

	private int objectSelected;

	public GameObject instantiator;
	public const int INVENTORY_SIZE = 2;
	
	public void OnEnable()
	{
		InitializeMenu();
	}

	public void InitializeMenu()
	{
		objectSelected = 0;
		_quantity = new int[INVENTORY_SIZE];
		
		for (int i = 0; i < selectors.Length; i++)
		{
			if (i != objectSelected)
			{
				selectors[i].SetActive(false);
			}
			else
			{
				selectors[i].SetActive(true);
			}
		}
		
		int[] array = {1, 10};
		SetQuantityToAll(array);
		UpdateQuantityInfo();
	}

	public void IncrementSelect()
	{
		if (objectSelected < selectors.Length-1)
		{
			selectors[objectSelected].SetActive(false);
			objectSelected++;
			selectors[objectSelected].SetActive(true);
		}
	}

	public void DecrementSelect()
	{
		
		if (objectSelected > 0)
		{
			selectors[objectSelected].SetActive(false);
			objectSelected--;
			selectors[objectSelected].SetActive(true);
		}
		
	}

	public void InstantiateItem()
	{
		if (_quantity[objectSelected] > 0)
		{
			_quantity[objectSelected]--;
			UpdateQuantityInfo();
			Transform tr = instantiator.GetComponent<Transform>();
			Instantiate(items[objectSelected], tr.position+new Vector3(0,0,0), Quaternion.identity);
		}
	}

	public void UpdateQuantityInfo()
	{
		for (int i = 0; i < quantityInfo.Length; i++)
		{
			quantityInfo[i].text = "x" + _quantity[i];
		}
	}

	public void AddQuantity(int item, int quantity)
	{
		_quantity[item] += quantity;
		UpdateQuantityInfo();
	}

	public void SetQuantityToAll(int[] qt)
	{
		for (int i = 0; i < _quantity.Length; i++)
		{
			_quantity[i] = qt[i];
		}
		UpdateQuantityInfo();
	}

	public int GetQuantity(int i)
	{
		return _quantity[i];
	}

	public void SetQuantity(int item, int quantity)
	{
		_quantity[item] = quantity;
	}
	
}
