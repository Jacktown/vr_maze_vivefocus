﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    
    private Collider[] surroundings;
    private Transform tr;
    
    public MoveRig scMR;
    public GameObject transitionMenu;
    public MapGeneratation scMG;
    public UserControllerListener scUCL;
    public GameObject inventoryMenu;

    private void Start()
    {
        tr = GetComponent<Transform>();
    }

    private void Update()
    {
        
        surroundings = Physics.OverlapSphere(tr.position, 0.5f);
        CheckIfExit();
    }

    private void CheckIfExit()
    {
        int i = 0;
        if (surroundings != null)
        {
            while (i < surroundings.Length && !(surroundings[i].gameObject.CompareTag("Exit")))
            {
                i++;
            }


            if (i < surroundings.Length && surroundings[i].gameObject.CompareTag("Exit"))
            {
                ExitMaze();
            }
        }
    }

    private void ExitMaze()
    {
        scMR.RigToMenu();
        transitionMenu.SetActive(true);
        inventoryMenu.SetActive(false);
        scMG.DestroyMaze();
        scUCL.ActivateTool(false);
        scUCL.ActivatePause(false);
        
        /// TODO
        /// DestroyItems()
    }
    
/*
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Exit"))
        {
            scMR.RigToMenu();
        }
    }
*/
}
